# Angular2 Minimalist Starter

This kit will be customized for my needs but it is probably the best Angular 2 starter kit I have found.

The repo can be found at [https://github.com/rogerpadilla/angular2-minimalist-starter](https://github.com/rogerpadilla/angular2-minimalist-starter)

## Quick start
```bash
# 1. Clone / Download the repo.
# 2. Run command: npm install
# 3. Run command: npm start
```
